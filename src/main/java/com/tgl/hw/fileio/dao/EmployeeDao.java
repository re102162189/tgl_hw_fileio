package com.tgl.hw.fileio.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;
import com.tgl.hw.fileio.model.Employee;

/**
 * 
 * @author kite.chen Designed for data access
 *
 */
public class EmployeeDao {

  private Map<Long, Employee> employeeMap;

  private AtomicLong id = new AtomicLong(1);

  public EmployeeDao(String filePath) {
    employeeMap = new HashMap<>();

    try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {

      String line;
      String[] data;
      while ((line = reader.readLine()) != null) {
        data = line.split(" ");

        // TODO any invalid input handle?
        //
        Employee employee = new Employee();
        employee.setId(id.getAndIncrement());
        employee.setHeight(Integer.parseInt(data[0]));
        employee.setWeight(Integer.parseInt(data[1]));
        employee.setEngName(data[2]);
        employee.setChName(data[3]);
        employee.setPhone(data[4]);
        employee.setEmail(data[5]);
        employee.setBmi(employee.getWeight() / Math.pow((employee.getHeight() / 100), 2));

        employeeMap.put(employee.getId(), employee);
      }
    } catch (IOException e1) {
      e1.printStackTrace();
    }
    
    System.out.printf("=============init employee data, size: %s==========\n", this.size());
  }

  public Employee add(Employee employee) {
    employee.setId(id.getAndIncrement());
    employeeMap.put(employee.getId(), employee);
    return employee;
  }

  public boolean delete(long employeeId) {
    if (employeeMap.containsKey(employeeId)) {
      employeeMap.remove(employeeId);
      return true;
    } else {
      return false;
    }
  }

  public Employee update(Employee employee) {
    employeeMap.put(employee.getId(), employee);
    return employeeMap.get(employee.getId());
  }

  public Employee search(Employee.Search factor, String searchVal) {
    Employee employee;
    switch (factor) {
      case ID:
        employee = employeeMap.get(Long.parseLong(searchVal));
        break;
      default:
        employee = new Employee();
        break;
    }
    return employee;
  }

  public List<Employee> sort(Employee.Sort factor) {
    List<Employee> resultList;
    switch (factor) {
      case PHONE:
        Map<String, Employee> resultMap = new TreeMap<>();
        for (Map.Entry<Long, Employee> empEntry : employeeMap.entrySet()) {
          Employee employee = empEntry.getValue();
          resultMap.put(employee.getPhone(), employee);
        }
        resultList = new ArrayList<>(resultMap.values());
        break;
      default:
        resultList = new ArrayList<>();
        break;
    }
    return resultList;
  }
  
  public Employee max(Employee.Max factor) {
    Employee result;
    switch (factor) {
      case HEIGHT:
        TreeMap<String, Employee> resultMap = new TreeMap<>();
        for (Map.Entry<Long, Employee> empEntry : employeeMap.entrySet()) {
          Employee employee = empEntry.getValue();
          resultMap.put(employee.getPhone(), employee);
        }
        result = resultMap.firstEntry().getValue();
        break;
      default:
        result = new Employee();
        break;
    }
    return result;
  }
  
  public Employee min(Employee.Min factor) {
    Employee result;
    switch (factor) {
      case HEIGHT:
        TreeMap<String, Employee> resultMap = new TreeMap<>();
        for (Map.Entry<Long, Employee> empEntry : employeeMap.entrySet()) {
          Employee employee = empEntry.getValue();
          resultMap.put(employee.getPhone(), employee);
        }
        result = resultMap.lastEntry().getValue();
        break;
      default:
        result = new Employee();
        break;
    }
    return result;
  }

  public int size() {
    return employeeMap.size();
  }
}
