package com.tgl.hw.fileio.model;

/**
 * 
 * @author kite.chen
 * Designed for wrapping Employee properties
 *
 */
public class Employee {

  public enum Search {
    ID
  }
  
  public enum Sort {
    PHONE
  }
  
  public enum Max {
    HEIGHT
  }
  
  public enum Min {
    HEIGHT
  }

  private long id;
  private double height;
  private double weight;
  private String engName;
  private String chName;
  private String phone;
  private String email;
  private double bmi;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(double height) {
    this.height = height;
  }

  public double getWeight() {
    return weight;
  }

  public void setWeight(double weight) {
    this.weight = weight;
  }

  public String getEngName() {
    return engName;
  }

  public void setEngName(String engName) {
    this.engName = engName;
  }

  public String getChName() {
    return chName;
  }

  public void setChName(String chName) {
    this.chName = chName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public double getBmi() {
    return bmi;
  }

  public void setBmi(double bmi) {
    this.bmi = bmi;
  }

  @Override
  public String toString() {
    return "Employee [id=" + id + ", height=" + height + ", weight=" + weight + ", engName="
        + engName + ", chName=" + chName + ", phone=" + phone + ", email=" + email + ", bmi=" + bmi
        + "]";
  }
}
