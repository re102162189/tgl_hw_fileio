package com.tgl.hw.fileio;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import com.tgl.hw.fileio.controller.EmployeeController;
import com.tgl.hw.fileio.model.Employee;

/**
 * 
 * @author kite.chen Designed main demo flow
 *
 */
public class FileIODemo {

  private EmployeeController employeeController;

  public static void main(String[] args) {
    try (InputStream input =
        FileIODemo.class.getClassLoader().getResourceAsStream("config.properties")) {

      Properties prop = new Properties();
      prop.load(input);
      String filePath = prop.getProperty("employee.data");

      FileIODemo fileIODemo = new FileIODemo();
      fileIODemo.employeeController = new EmployeeController(filePath);

      System.out.println("\n=============search case [by ID] fail==========");
      fileIODemo.search("100");

      System.out.println("\n=============search case [by ID] success==========");
      fileIODemo.search("3");

      System.out.println("\n=============add case success==========");
      Employee employee = new Employee();
      employee.setHeight(173.5);
      employee.setWeight(73);
      employee.setEngName("Kite Chen");
      employee.setChName("陳銘凱");
      employee.setPhone("6926");
      employee.setEmail("kitechen@transglobe.com.tw");
      fileIODemo.add(employee);

      System.out.println("\n=============update case success==========");
      String employeeId = "57";
      employee = fileIODemo.employeeController.search(Employee.Search.ID, employeeId);
      employee.setHeight(180);
      employee = fileIODemo.employeeController.update(employee);
      System.out.println("employee updated: " + employee);

      System.out.println("\n=============delete [by ID] case fail==========");
      fileIODemo.delete(100);

      System.out.println("\n=============delete [by Id] case success==========");
      fileIODemo.delete(57);

      System.out.println("\n=============sort [by phone] case (print only first 10)==========");
      List<Employee> resultList = fileIODemo.employeeController.sort(Employee.Sort.PHONE);
      int showCnt = 10;
      for (Employee result : resultList) {
        if (showCnt > 0) {
          System.out.printf("%s, %s \n", result.getChName(), result.getPhone());
        } else {
          break;
        }
        showCnt--;
      }
      
      System.out.println("\n=============max [by height] case ==========");
      employee = fileIODemo.employeeController.max(Employee.Max.HEIGHT);
      System.out.printf("%s, %s \n", employee.getChName(), employee.getHeight());
      
      System.out.println("\n=============min [by height] case ==========");
      employee = fileIODemo.employeeController.min(Employee.Min.HEIGHT);
      System.out.printf("%s, %s \n", employee.getChName(), employee.getHeight());

    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public void search(String employeeId) {
    Employee employee = employeeController.search(Employee.Search.ID, employeeId);
    if (employee != null) {
      System.out.println("search result by Id: " + employee);
    } else {
      System.out.printf("employee Id[%s] not found\n", employeeId);
    }
  }

  public void add(Employee employee) {
    employee = employeeController.add(employee);
    System.out.println("employee added: " + employee);
  }

  public void delete(long deleteId) {
    System.out.println("employee size defore delete: " + employeeController.size());
    boolean isDeleted = employeeController.delete(deleteId);
    if (isDeleted) {
      System.out.printf("employee deleted by Id: %s\n", deleteId);
    } else {
      System.out.printf("employee Id[%s] not found\n", deleteId);
    }
    System.out.println("employee size after delete: " + employeeController.size());
  }
}
