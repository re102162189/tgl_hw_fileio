package com.tgl.hw.fileio.service;

import java.util.List;
import com.tgl.hw.fileio.dao.EmployeeDao;
import com.tgl.hw.fileio.model.Employee;

/**
 * 
 * @author kite.chen
 * Designed for business logic 
 *
 */
public class EmployeeService {
  
  private EmployeeDao employeeDao;
  
  public EmployeeService(String filePath) {
    employeeDao = new EmployeeDao(filePath);
  }

  public Employee add(Employee employee) {
    // it might be better handled inside Employee model ?
    //
    employee.setBmi(employee.getWeight() / Math.pow((employee.getHeight() / 100), 2));
    return employeeDao.add(employee);
  }

  public boolean delete(long employeeId) {
    return employeeDao.delete(employeeId);
  }

  public Employee update(Employee employee) { 
    return employeeDao.update(employee);
  }

  public Employee search(Employee.Search factor, String searchVal) {
    return employeeDao.search(factor, searchVal);
  }
  
  public List<Employee> sort(Employee.Sort factor) {
    return employeeDao.sort(factor);
  }
  
  public Employee max(Employee.Max factor) {
    return employeeDao.max(factor);
  }
  
  public Employee min(Employee.Min factor) {
    return employeeDao.min(factor);
  }
  
  public int size() {
    return employeeDao.size();
  }
}
