package com.tgl.hw.fileio.controller;

import java.util.List;
import com.tgl.hw.fileio.model.Employee;
import com.tgl.hw.fileio.service.EmployeeService;

/**
 * 
 * @author kite.chen
 * Designed for function router 
 *
 */
public class EmployeeController {
  
  private EmployeeService employeeService;
  
  public EmployeeController(String filePath) {
    employeeService = new EmployeeService(filePath);
  }
  
  public Employee add(Employee employee) {
    // it might be better handled invalid input here ?
    //
    return employeeService.add(employee);
  }

  public boolean delete(long employeeId) {
    return employeeService.delete(employeeId);
  }

  public Employee update(Employee employee) {
    return employeeService.update(employee);
  }
  
  public Employee max(Employee.Max factor) {
    return employeeService.max(factor);
  }
  
  public Employee min(Employee.Min factor) {
    return employeeService.min(factor);
  }

  public Employee search(Employee.Search factor, String searchVal) {
    return employeeService.search(factor, searchVal);
  }
  
  public List<Employee> sort(Employee.Sort factor) {
    return employeeService.sort(factor);
  }
  
  public int size() {
    return employeeService.size();
  }
}
