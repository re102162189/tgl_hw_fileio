**FileIO demo for TGL assignment**  
---
1. designed 3-tiers (controller, service, dao) for future assignment scalability  
2. demo few features concept, e.g. search by Id, sort by phone...etc. complete more features on your own  
3. prepare your input file path and configure it in /src/main/resources/config.properties  
4. run the Main class /src/main/java/com/tgl/hw/fileio/FileIODemo to see the demo